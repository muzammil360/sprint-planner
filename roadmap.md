
# General idea
You write your issue description in a template. Python code simply makes issues based on csv data. The goal is to make bulk issue creation in git sites faster and simpler.   


# Phase 1
**Goal**: create minimalistic application that gets the job done
- move hardcoded vars to config
- make script stable
  - debug why it takes so much time to push
  - no useless/extra logs
- Write getting stated 


# Phase 2
**Goal**: Exercise SW engineering design principals to make an extensible, reusable project
- redesign application (conceptual/technical)
- separate out main() and library code
- add support for gitlab, github and bitbucket
- add UI
- package with installer